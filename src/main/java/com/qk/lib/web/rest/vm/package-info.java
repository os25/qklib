/**
 * View Models used by Spring MVC REST controllers.
 */
package com.qk.lib.web.rest.vm;
